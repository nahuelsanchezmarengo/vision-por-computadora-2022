import sys
import cv2
import numpy as np
import os           # para obtener la extension del filename
import math

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)


xi = 0
yi = 0
xf = 0
yf = 0

drawing = False     # True si está dibujando (arrastrando)
selecting = False   # True si está en proceso de selección (dibujó pero no guardó aun)


def fname(arg):
    pass
def areaSelection(event, x, y, flags, param):
    global xi, yi, xf, yf, selecting, drawing

    img_w = len(img[0])
    img_h = len(img)

    if selecting is True and drawing is True:
        img_temp[:] = img[:]

    if event == cv2.EVENT_LBUTTONDOWN and selecting is False:
        # los valores de x e y se asignan, si están dentro del rango válido
        # evita error al arrastrar por fuera de las dimensiones de la ventana
        xi = 0 if x < 0 else x if x <= img_w else img_w
        yi = 0 if y < 0 else y if y <= img_h else img_h
        # yi = y if y >= 0 else 0
        selecting = True
        drawing = True
    elif event == cv2.EVENT_MOUSEMOVE and selecting is True and drawing is True:
        xf = 0 if x < 0 else x if x <= img_w else img_w
        yf = 0 if y < 0 else y if y <= img_h else img_h
        cv2.rectangle(img_temp, (xi, yi), (xf, yf), red, 1)            # dibuja rectángulo temp
    elif event == cv2.EVENT_LBUTTONUP and selecting is True and drawing is True:
        xf = 0 if x < 0 else x if x <= img_w else img_w
        yf = 0 if y < 0 else y if y <= img_h else img_h
        cv2.rectangle(img_temp, (xi, yi), (xf, yf), red, 1)           # dibuja rectángulo final
        cv2.imshow('Seleccione imagen', img_temp)
        print("'g': guarda la porción de imagen seleccionada")
        print("'r': nueva selección")
        print("'e': trasnformación euclideana")
        print("'s': trasnformación de similaridad")
        print("'q': finalizar")
        drawing = False

# -------------------------------------------------------------------------------

if len(sys.argv) > 1:      # verifica que se haya pasado como argumento
    filename = sys.argv[1]
else:
    print('Pasar el nombre de archivo como argumento')
    sys.exit(0)
if not os.path.isfile(os.path.abspath(filename)):   # verifica que exista ese archivo, antes de cargarlo
    print(filename, ' no existe en el directorio.')
    sys.exit(0)

extension = os.path.splitext(filename)[1]   # obtiene extensión de la imagen
num_saved = 0                               # numero identificador de imagen guardada

img = cv2.imread(filename , cv2.IMREAD_UNCHANGED)       # imagen de entrada
img_temp = cv2.imread(filename , cv2.IMREAD_UNCHANGED)  # img_temp será la imagen sin el rectángulo de seleccion dibujado

cv2.namedWindow('Seleccione imagen')
cv2.setMouseCallback('Seleccione imagen', areaSelection)

print('Seleccione un área de la imagen a recortar.')

while(1):
    if selecting is True:
        cv2.imshow('Seleccione imagen', img_temp)
    else:
        cv2.imshow('Seleccione imagen', img)

    key = cv2.waitKey(1) & 0xFF

    # tecla q: termina programa
    if key == ord('q'):
        break
    # tecla r: descarta selección actual
    elif key == ord('r') and (drawing is True or selecting is True):
        selecting = False
        print('Seleccione un área de la imagen a recortar.')
    # tecla g: guarda área seleccionada
    elif key == ord('g') and drawing is False and selecting is True:
        selecting = False
        num_saved = num_saved + 1
        X = (xi, xf); Y= (yi, yf)
        img_out = img[min(Y):max(Y), min(X):max(X), :]          # guarda área en img_out
        filename_out = 'seleccion' + str(num_saved) + extension # ej: seleccion1.jpg
        if cv2.imwrite(filename_out, img_out):
            print('Selección guardada correctamente en: ', filename_out)
        else:
            print('Error durante el guardado')
            break
    # tecla e: aplica transformación euclideana sobre el área seleccionada
    elif key == ord('e') and drawing is False and selecting is True:
        selecting = False
        num_saved = num_saved + 1

        x_e = int(input('Ingrese valor del desplazamiento en x: '))         # desplazamiento en x
        y_e = int(input('Ingrese valor del desplazamiento en y: '))         # desplazamiento en y
        angle_deg_e = int(input('Ingrese ángulo a rotar: '))                # angulo a rotar
        angle_rad_e = math.radians(angle_deg_e)

        # matriz de transfomracion euclideana
        H_e = np.array([[math.cos(angle_rad_e),     math.sin(angle_rad_e),  x_e],
                        [-math.sin(angle_rad_e),    math.cos(angle_rad_e),  y_e],
                        [0,                         0,                      1]])

        # matriz de transfomracion inversa
        H_e_inv = np.linalg.inv(H_e)

        (h, w, ch) = img.shape

        # fondo gris para la imagen de salida, con las mismas dimensiones de img
        img_out = np.ones((h, w, ch))*100

        for row in range(0, h):
            for col in range(0, w):
                # matriz extendida del punto de salida
                Pout = np.array([[col],
                                 [row],
                                 [1]])
                Pin = np.matmul(H_e_inv, Pout)

                Pin_x = int(round(Pin[0][0]))
                Pin_y = int(round(Pin[1][0]))

                # si Pin cae dentro del área seleccionada, se copia el contenido del pixel
                # de img en img_out
                if min(xi, xf) <= Pin_x < max(xi, xf) and min(yi, yf) <= Pin_y < max(yi, yf):
                    img_out[row][col][:] = img[Pin_y][Pin_x][:]

        filename_out = 'euclideana' + str(num_saved) + extension    # ej: seleccion1.jpg
        if cv2.imwrite(filename_out, img_out):
            print('Transformación guardada correctamente en: ', filename_out)
        else:
            print('Error durante el guardado')
            break

    # tecla s: aplica transformación de similaridad sobre el área seleccionada
    elif key == ord('s') and drawing is False and selecting is True:
        selecting = False
        num_saved = num_saved + 1

        x_s = int(input('Ingrese valor del desplazamiento en x: '))  # desplazamiento en x
        y_s = int(input('Ingrese valor del desplazamiento en y: '))  # desplazamiento en y
        angle_deg_s = int(input('Ingrese ángulo a rotar: '))  # angulo a rotar
        angle_rad_s = math.radians(angle_deg_s)
        s_s = float(input('Ingrese valor del escalado: '))

        # matriz de transfomracion euclideana
        H_s = np.array([[s_s*math.cos(angle_rad_s),     s_s*math.sin(angle_rad_s),  x_s],
                        [-s_s*math.sin(angle_rad_s),    s_s*math.cos(angle_rad_s),  y_s],
                        [0,                             0,                          1]])

        # matriz de transfomracion inversa
        H_s_inv = np.linalg.inv(H_s)

        (h, w, ch) = img.shape

        # fondo negro para la imagen de salida, con las mismas dimensiones de img
        img_out = np.ones((h, w, ch)) * 100

        print(h, w, ch)

        for row in range(0, h):
            for col in range(0, w):
                # matriz extendida del punto de salida
                Pout = np.array([[col],
                                 [row],
                                 [1]])
                Pin = np.matmul(H_s_inv, Pout)

                Pin_x = int(round(Pin[0][0]))
                Pin_y = int(round(Pin[1][0]))

                # si Pin cae dentro del área seleccionada, se copia el contenido del pixel
                # de img en img_out
                if Pin_x >= min(xi, xf) and Pin_x < max(xi, xf) and Pin_y >= min(yi, yf) and Pin_y < max(yi, yf):
                    img_out[row][col][:] = img[Pin_y][Pin_x][:]

        filename_out = 'similaridad' + str(num_saved) + extension  # ej: seleccion1.jpg
        if cv2.imwrite(filename_out, img_out):
            print('Transformación guardada correctamente en: ', filename_out)
        else:
            print('Error durante el guardado')
            break

cv2.destroyAllWindows()
