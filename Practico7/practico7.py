import sys
import cv2
import numpy as np
import os           # para obtener la extension del filename

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)
yellow = (0, 255, 255)

p1 = ()
p2 = ()
p3 = ()
p4 = ()

point_counter = 0


selecting = True   # True si está en proceso de selección de puntos


def mouse_actions(event, x, y, flags, param):
    global p1, p2, p3, p4, selecting, point_counter, img_temp

    if event == cv2.EVENT_LBUTTONDOWN and selecting is True:
        if point_counter == 0:
            p1 = (x, y)
            cv2.circle(img_temp, p1, 3, blue, -3)
            point_counter += 1
        elif point_counter == 1:
            p2 = (x, y)
            cv2.circle(img_temp, p2, 3, green, -3)
            point_counter += 1
        elif point_counter == 2:
            p3 = (x, y)
            cv2.circle(img_temp, p3, 3, red, -3)
            point_counter += 1
            # calcula el 4to punto del paralelogramo
            if p1[0] < p2[0]:
                p4x = p3[0] - abs(p1[0] - p2[0])
            else:
                p4x = p3[0] + abs(p1[0] - p2[0])
            if p1[1] < p2[1]:
                p4y = p3[1] - abs(p1[1] - p2[1])
            else:
                p4y = p3[1] + abs(p1[1] - p2[1])
            p4 = (p4x, p4y)

            cv2.circle(img_temp, p4, 3, yellow, -3)

            # dibuja el paralelogramo
            # poly = np.float32([p1, p2, p3, p4])
            # cv2.polylines(img_temp, poly, True, red, 1)
            print("'a': aplica transformación afin")
            print("'q': finalizar")
                
# -------------------------------------------------------------------------------


if len(sys.argv) == 3:          # verifica que se haya pasado como argumento
    filename1 = sys.argv[1]     # imagen inicial
    filename2 = sys.argv[2]     # imagen a incrustar
else:
    print('Pasar los nombres de archivos como argumentos')
    sys.exit(0)

if not os.path.isfile(os.path.abspath(filename1)):   # verifica que exista ese archivo, antes de cargarlo
    print(filename1, ' no existe en el directorio.')
    sys.exit(0)

if not os.path.isfile(os.path.abspath(filename2)):   # verifica que exista ese archivo, antes de cargarlo
    print(filename2, ' no existe en el directorio.')
    sys.exit(0)

extension = os.path.splitext(filename1)[1]   # obtiene extensión de la imagen
num_saved = 0                                # numero identificador de imagen guardada

img = cv2.imread(filename1, cv2.IMREAD_UNCHANGED)       # carga en img: imagen de entrada
img_temp = cv2.imread(filename1, cv2.IMREAD_UNCHANGED)  # carga en img_temp: imagen sin puntos de selección dibujados

img2 = cv2.imread(filename2, cv2.IMREAD_UNCHANGED)      # carga en img2: imagen a incrustar

cv2.namedWindow('Seleccione imagen')                    # crea la ventana de trabajo
cv2.setMouseCallback('Seleccione imagen', mouse_actions)

print('Seleccione 3 puntos con el mouse')

while 1:
    if selecting is True:
        cv2.imshow('Seleccione imagen', img_temp)
    else:
        cv2.imshow('Seleccione imagen', img_out)

    key = cv2.waitKey(1) & 0xFF

    # tecla q: termina programa
    if key == ord('q'):
        break

    # tecla a: aplica transformación afin
    if key == ord('a'):
        selecting = False
        (h, w, ch) = img.shape
        (h2, w2, ch2) = img2.shape

        input_pts = np.float32([[0, h2], [0, 0], [w2, 0]])
        output_pts = np.float32([p1, p2, p3])

        H_a = cv2.getAffineTransform(input_pts, output_pts)
        img2 = cv2.warpAffine(img2, H_a, (w, h))
        indexs = np.sum(img2, axis=2) != 0
        img_out = img[:]
        img_out[indexs] = img2[indexs]

cv2.destroyAllWindows()
