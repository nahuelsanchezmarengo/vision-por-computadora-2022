import sys
import cv2
import numpy as np

# Medidas campo de juego Estadio Monumental [m]
largoT = 105
anchoT = 68
largoAG = 16.5
anchoAG = 40.32
largoAC = 5.5
anchoAC = 18.32
radioC = 9.15

# Vertices area grande en metros
der_AG_1 = (largoT-largoAC, int((anchoT-anchoAC)/2))
der_AG_2 = ((largoT-largoAG), int((anchoT-anchoAG)/2))
der_AG_3 = ((largoT-largoAG), int((anchoT-anchoAG)/2+anchoAG))
der_AG_4 = ((largoT-largoAC), int((anchoT-anchoAC)/2+anchoAC))


# Escala pixel/metro
esc = 10

H = 0   # matriz de transformacion

p = [[], [], [], []]    # cuatro puntos conocidos del campo de juego a seleccionar
p_out = np.empty(0)
plyr1 = np.empty(0)
plyr2 = np.empty(0)

point_counter = 0
select_area = True      # True si está en proceso de selección de puntos del área
select_plyrs = False    # True si está en proceso de selección de jugadores


def mouse_actions_sel(event, x, y, flags, param):
    global p_out, plyr1, plyr2, H, H_inv, select_area, select_plyrs, point_counter, img, img_temp, img_temp2, img_out, img_out_temp

    if event == cv2.EVENT_LBUTTONDOWN:
        if select_plyrs is True:
            if not len(plyr1):
                plyr1 = p_out
                img = np.copy(img_temp)
                img_out = np.copy(img_out_temp)
            else:
                plyr2 = p_out
                select_plyrs = False
                distancia = plyr2[0] - plyr1[0]

                if distancia >= 0:
                    print('Habilitado. Distancia entre jugadores: ' + str(distancia) + ' m')
                else:
                    print('Offside. Distancia entre jugadores: ' + str(distancia) + ' m')
                img = np.copy(img_temp)
                img_out = np.copy(img_out_temp)

        if select_area is True:
            p[point_counter] = [x, y]
            cv2.circle(img_temp, tuple(p[point_counter]), 3, (255, 0, 0), -3)
            point_counter += 1
            img_temp2 = np.copy(img_temp)
            if point_counter == 4:
                # obtención de la matriz de transformación
                # los puntos de entrada son los puntos seleccionados manualmente
                # los puntos de salida son sus correspondientes puntos conocidos a través de las dimensiones del campo
                input_pts = np.float32(p)
                output_pts = np.float32([der_AG_1,
                                         der_AG_2,
                                         der_AG_3,
                                         der_AG_4])
                H = cv2.getPerspectiveTransform(input_pts, output_pts)
                H_inv = np.linalg.inv(H)
                img_temp = np.copy(img)
                img_temp2 = np.copy(img)
                select_area = False
                select_plyrs = True

    if event == cv2.EVENT_MOUSEMOVE:
        if select_area is True and point_counter > 0:
            img_temp = np.copy(img_temp2)
            cv2.line(img_temp, tuple(p[point_counter-1]), (x, y), (255, 0, 0), thickness=1)
            if point_counter == 3:
                cv2.line(img_temp, (x, y), tuple(p[0]), (255, 0, 0), thickness=1)

        if select_plyrs is True:
            img_out_temp = np.copy(img_out)
            img_temp = np.copy(img)
            p_out = pointPerspective((x, y), H)

            if len(plyr1):
                color = (255, 0, 0)
            else:
                color = (0, 0, 255)

            cv2.circle(img_temp, (x, y), 3, color, -3)
            cv2.circle(img_out_temp, tuple(map(lambda x: int(x * esc), p_out)), 3, color, -3)
            l1_pi = [p_out[0], 0]
            l1_pf = [p_out[0], anchoT]

            # traza línea en img_out
            cv2.line(img_out_temp,
                     tuple(map(lambda x: int(x * esc), l1_pi)),
                     tuple(map(lambda x: int(x * esc), l1_pf)),
                     color=color)

            # traza línea transformadas en img
            cv2.line(img_temp,
                     tuple(map(int, pointPerspective(l1_pi, H_inv))),
                     tuple(map(int, pointPerspective(l1_pf, H_inv))),
                     color=color)

            if len(plyr1):
                distancia = plyr1[0] - p_out[0]
                if distancia < 0:
                    cv2.putText(img_temp,
                                "Habilitado.({:.2f}m)".format(abs(distancia)),
                                (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2, cv2.LINE_AA)
                else:
                    cv2.putText(img_temp,
                                "Offside.({:.2f}m)".format(abs(distancia)),
                                (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2, cv2.LINE_AA)

def dibuja_campo(img_out):
    cv2.rectangle(img_out, (0, 0), (largoT*esc-1, anchoT*esc-1), color=(255, 0, 0), thickness=1)
    cv2.rectangle(img_out,
                  (0, int((anchoT-anchoAG)/2*esc)),
                  (int(largoAG*esc), int(((anchoT-anchoAG)/2+anchoAG)*esc)),
                  color=(255, 0, 0), thickness=1)
    cv2.rectangle(img_out,
                  (0, int((anchoT - anchoAC) / 2 * esc)),
                  (int(largoAC * esc), int(((anchoT - anchoAC) / 2 + anchoAC) * esc)),
                  color=(255, 0, 0), thickness=1)
    cv2.rectangle(img_out,
                  (largoT*esc, int((anchoT - anchoAG) / 2 * esc)),
                  (int((largoT-largoAG)*esc), int(((anchoT - anchoAG) / 2 + anchoAG) * esc)),
                  color=(255, 0, 0), thickness=1)
    cv2.rectangle(img_out,
                  (largoT*esc, int((anchoT - anchoAC) / 2 * esc)),
                  (int((largoT-largoAC)*esc), int(((anchoT - anchoAC) / 2 + anchoAC) * esc)),
                  color=(255, 0, 0), thickness=1)
    cv2.line(img_out, (int((largoT * esc)/2), 0), (int((largoT * esc)/2), anchoT*esc), color=(255, 0, 0), thickness=1)
    cv2.circle(img_out, (int((largoT * esc) / 2), int((anchoT * esc) / 2)), int(radioC*esc), color=(255, 0, 0), thickness=1)


def pointPerspective(src, H):
    p_in = np.array([[src[0]],
                     [src[1]],
                     [1]])
    p_out = np.matmul(H, p_in)
    p_out = p_out / p_out[2]
    return p_out[0, 0], p_out[1, 0]

# -------------------------------------------------------------------------------


img = cv2.imread("frame.png", cv2.IMREAD_UNCHANGED)       # carga en img: imagen de entrada
img_temp = np.copy(img)  # carga en img_temp: imagen sin puntos de selección dibujados
img_temp2 = np.copy(img)
img_out = np.ones((anchoT*esc, largoT*esc, 3), dtype=np.uint8)*255      # imagen del campo de juego virtual

dibuja_campo(img_out)
img_out_temp = np.copy(img_out)

cv2.namedWindow('Ventana de seleccion')   # crea la ventana de seleccion
cv2.namedWindow('Ventana de salida')      # crea la ventana de visulización de resultados
cv2.setMouseCallback('Ventana de seleccion', mouse_actions_sel)

print('Seleccionar los 4 puntos conocidos del campo de juego')

while 1:
    cv2.imshow('Ventana de seleccion', img_temp)
    cv2.imshow('Ventana de salida', img_out_temp)

    if select_plyrs is True:
        cv2.imshow('Ventana de seleccion', img_temp)

    key = cv2.waitKey(33) & 0xFF

    # tecla q: termina programa
    if key == ord('q'):
        break

cv2.destroyAllWindows()
