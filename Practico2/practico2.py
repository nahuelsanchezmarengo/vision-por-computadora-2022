#!/usr/bin/env python
#-*-coding: utf-8 -*-

import sys
import cv2
import numpy as np


img = cv2.imread('hoja.png', cv2.IMREAD_GRAYSCALE)
xydimension = img.shape

if (len(sys.argv) > 1):
    if (int(sys.argv[1]) >= 0 and int(sys.argv[1]) <= 255):
        umbral = int(sys.argv[1])
    else:
        print('Ingrese un valor ENTERO de umbral')
        exit(1)
else:
    print('Ingrese un valor entero de umbral')
    exit(1)

# for row in range(0, xydimension[0]):
#     for col in range(0, xydimension[1]):
#         if img[row, col] <= umbral:
#             img[row, col] = 0
#         else:
#             img[row, col] = 255

img[img < umbral] = 0



#cv2.imwrite('resultado.png', img)
cv2.imshow('Resultado', img)
cv2.waitKey(0)
cv2.destroyAllWindows()


# cv2.imwrite('resultado.png', img )
