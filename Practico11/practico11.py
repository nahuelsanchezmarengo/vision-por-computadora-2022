import numpy as np
import cv2

MIN_MATCH_COUNT = 10
scale = 10

img1 = cv2.imread('img1.jpg')
img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.imread('img2.jpg')
img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

sift = cv2.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1_gray, None)
kp2, des2 = sift.detectAndCompute(img2_gray, None)

# cv2.drawKeypoints(img1, kp1, img1)
# cv2.drawKeypoints(img2, kp2, img2)
cv2.imshow('imagen1', img1)
cv2.imshow('imagen2', img2)

matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)
good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

print(len(good))

if len(good) > MIN_MATCH_COUNT:
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)
else:
    print('Not enough matches')

wimg2 = cv2.warpPerspective(img2, H, (img1.shape[1], img1.shape[0]))
# cv2.imshow('wimg2', wimg2)

alpha = 0.5
blend = np.array(wimg2*alpha+img1*(1-alpha), dtype=np.uint8)
cv2.imshow('blend', blend)
key = cv2.waitKey(0) & 0xFF

if key == ord('q'):
    cv2.destroyAllWindows()

cv2.destroyAllWindows()
