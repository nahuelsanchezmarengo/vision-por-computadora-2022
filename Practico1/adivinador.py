import random

def adivina(intentos):
    numero = random.randint(0, 5)
    for i in range(1,intentos+1):
        ingreso=int(input('Ingrese un entero: '))
        if ingreso==numero:
            print('¡Acertaste en el intento nº {}!' .format(i) )
            return 0
    print('¡Perdiste!')

intentos=int(input('Cantidad de intentos: '))
adivina(intentos)
