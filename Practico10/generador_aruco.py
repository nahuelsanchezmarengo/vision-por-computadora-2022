import cv2 as cv
import numpy as np

diccionario = cv.aruco.Dictionary_get(cv.aruco.DICT_4X4_50)

imagen = np.zeros((200, 200), dtype = np.uint8)

for i in range(0, 50):
    imagen = cv.aruco.drawMarker(diccionario, i, 200, imagen, 1)
    cv.imshow("ventana", imagen)
    cv.imwrite("4x4_"+str(i)+".png", imagen)
    cv.waitKey(0)
cv.destroyAllWindows()
