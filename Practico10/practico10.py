import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# video de prueba a procesar
cap = cv.VideoCapture('tmp/video_test.mp4')
fps = cap.get(cv.CAP_PROP_FPS)

# se cargan las matrices de camara y distorción generadas con el script calibracion.py
mtx = np.load("tmp/mtx.npy")
dist = np.load("tmp/dist.npy")

markerLength = 10   # [cm] - dimensión del marcador a detectar

dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_4X4_50)
parameters = cv.aruco.DetectorParameters_create()

# vista inicial de los ejes 3D
elev = 100
azim = 270

lim = 70   # limites ejes 3D [cm]

fig = plt.figure(figsize=(7, 7)); ax = Axes3D(fig)
fig2, ax2 = plt.subplots(figsize=(7, 7))
X = []
Y = []
Z = []
t = []

while cap.isOpened():
    ret, frame = cap.read()
    corners, ids, rejected = cv.aruco.detectMarkers(frame, dictionary, parameters=parameters)
    if ids is not None:
        rvec, tvec, trash = cv.aruco.estimatePoseSingleMarkers(corners, markerLength, mtx, dist)
        frame = cv.aruco.drawDetectedMarkers(frame, corners, ids)
        # frame = cv.aruco.drawAxis(frame, mtx, dist, rvec, tvec, 5)

        R = cv.Rodrigues(rvec)[0]
        R_T = R.T
        T = tvec[0].T
        xyz = np.dot(R_T, -T).squeeze()
        x, y, z = xyz
        X.append(x)
        Y.append(y)
        Z.append(z)

        # plot 3D ---------------------------------------------------------------------------------
        ax.view_init(elev=elev, azim=azim)
        ax.set_xlabel("x", c='b'); ax.set_ylabel("y", c='g'); ax.set_zlabel("z", c='r')
        ax.set_xlim(-lim, lim); ax.set_ylim(-lim, lim); ax.set_zlim(-lim, lim)
        # dibuja ejes
        ax.quiver(0, 0, 0, 1, 0, 0, length=markerLength, color="b")
        ax.quiver(0, 0, 0, 0, 1, 0, length=markerLength, color="g")
        ax.quiver(0, 0, 0, 0, 0, 1, length=markerLength, color="r")
        ax.quiver(0, 0, 0, 1, 0, 0, length=lim, color="b", arrow_length_ratio=0, linewidth=0.3)
        ax.quiver(0, 0, 0, 0, 1, 0, length=lim, color="g", arrow_length_ratio=0, linewidth=0.3)
        ax.quiver(0, 0, 0, 0, 0, 1, length=lim, color="r", arrow_length_ratio=0, linewidth=0.3)
        # posición cámara
        ax.scatter(x, y, z, c='r')

        # plot 2D ---------------------------------------------------------------------------------
        ax2.plot(X, c='b')
        ax2.plot(Y, c='g')
        ax2.plot(Z, c='r')
        ax2.legend(['X', 'Y', 'Z'], loc=1)
        ax2.set_xlabel('Frame')
        ax2.set_ylabel('[cm]')
        # convierte plt en numpy para graficar
        fig.canvas.draw()
        fig2.canvas.draw()
        img_out = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
        img_out = img_out.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        img_out2 = np.frombuffer(fig2.canvas.tostring_rgb(), dtype=np.uint8)
        img_out2 = img_out2.reshape(fig2.canvas.get_width_height()[::-1] + (3,))
        ax.clear()
        ax2.clear()

    cv.imshow('Video entrada', cv.resize(frame, (960, 540)))
    cv.imshow('Vista 3D', img_out)
    cv.imshow('Vista 2D', img_out2)
    if cv.waitKey(10) & 0xFF == ord('q'):
        break

cv.destroyAllWindows()
