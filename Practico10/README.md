El script `practico10.py` carga el video `tmp/video_test.mp4` y grafica una vista tridimensional de la posición de la cámara respecto del aruco detectado.

La calibración de la cámara con que se realizó el video se realizó con el script `calibracion.py` y las matrices de cámara y distorsión son guardadas en `tmp/mtx.npy` y `tmp/dist.npy` respectivamente.





