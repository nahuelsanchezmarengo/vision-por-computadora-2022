import sys
import cv2
import numpy as np
import os  # para obtener la extension del filename

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)
yellow = (0, 255, 255)

p1 = ()
p2 = ()
p3 = ()
p4 = ()

point_counter = 0

selecting = True  # True si está en proceso de selección de puntos


def mouse_actions(event, x, y, flags, param):
    global p1, p2, p3, p4, selecting, point_counter, img_temp

    if event == cv2.EVENT_LBUTTONDOWN and selecting is True:
        if point_counter == 0:
            p1 = (x, y)
            cv2.circle(img_temp, p1, 3, blue, -3)
            point_counter += 1
        elif point_counter == 1:
            p2 = (x, y)
            cv2.circle(img_temp, p2, 3, green, -3)
            point_counter += 1
        elif point_counter == 2:
            p3 = (x, y)
            cv2.circle(img_temp, p3, 3, red, -3)
            point_counter += 1
        elif point_counter == 3:
            p4 = (x, y)
            cv2.circle(img_temp, p4, 3, yellow, -3)
            point_counter += 1
            print("'h': aplica homografía (rectifica imagen)")
            print("'q': finalizar")


# -------------------------------------------------------------------------------


if len(sys.argv) == 2:          # verifica que se haya pasado como argumento
    filename1 = sys.argv[1]     # imagen a rectificar
else:
    print('Pasar los nombres de archivos como argumentos')
    sys.exit(0)

if not os.path.isfile(os.path.abspath(filename1)):  # verifica que exista ese archivo, antes de cargarlo
    print(filename1, ' no existe en el directorio.')
    sys.exit(0)

extension = os.path.splitext(filename1)[1]  # obtiene extensión de la imagen
num_saved = 0                               # numero identificador de imagen guardada

img = cv2.imread(filename1, cv2.IMREAD_UNCHANGED)  # carga en img: imagen de entrada
img_temp = cv2.imread(filename1, cv2.IMREAD_UNCHANGED)  # carga en img_temp: imagen sin puntos de selección dibujados

cv2.namedWindow('Seleccione imagen')  # crea la ventana de trabajo
cv2.setMouseCallback('Seleccione imagen', mouse_actions)

print('Seleccione 4 puntos con el mouse: en sentido horario y comenzando'
      ' por el que considere como ángulo inferior izquierdo')
print('------------\n'
      '--2--→--3---\n'
      '------------\n'
      '--↑-----↓---\n'
      '------------\n'
      '--1-----4---\n'
      '------------\n')

while 1:
    if selecting is True:
        cv2.imshow('Seleccione imagen', img_temp)
    else:
        cv2.imshow('Seleccione imagen', img_out)

    key = cv2.waitKey(1) & 0xFF

    # tecla q: termina programa
    if key == ord('q'):
        break

    # tecla h: aplica transformación de homografía
    if key == ord('h'):
        selecting = False
        (h, w, ch) = img.shape
        # h=20; w=13.5
        input_pts = np.float32([p1, p2, p3, p4])
        output_pts = np.float32([[0, h], [0, 0], [w, 0], [w, h]])

        H_a = cv2.getPerspectiveTransform(input_pts, output_pts)
        img_out = cv2.warpPerspective(img, H_a, (w, h))

cv2.destroyAllWindows()